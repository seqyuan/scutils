#!/usr/bin/env python
# coding: utf-8 -*- 

import pandas as pd
import matplotlib
#matplotlib.use('Agg')
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from matplotlib.patches import Rectangle
from mpl_toolkits.mplot3d import Axes3D


file = '~/Desktop/mqc/out_df.mat'
df = pd.read_csv(file,header=0, index_col = 0, encoding='utf-8',sep="\t")

goodcolors = ['#004DA1','#F7CA18','#4ECDC4','#F9690E','#B35AA5','#7DCDF3','#0080CC','#F29F41','#DE6298','#C4EFF6','#E7C3FC','#81CFE0','#BDC3C7','#EDC0D3','#E5EF64','#4ECDC4','#168D7C','#103652','#D2484C','#E79D01','#C4EFF6','#C8F7C5','#FCECBB','#F9B7B2',]
cnames =  goodcolors + sorted(list(matplotlib.colors.cnames.keys()))[::2][::-1]

cnames = cnames[2:]

markers = ["v","o","^",",","p","H"]

class_1 = sorted(df['celltype'].unique())
df['Gcol'] = cnames[0]
df['Mark'] = markers[1]

for i,v in enumerate(class_1):
    df.loc[df[df['celltype']==v].index,'Gcol'] = cnames[i]
#celltype
fig = plt.figure(figsize=(6, 6))
[ax1_x, ax1_y, ax1_w, ax1_h] = [0.03,0.03,0.7,0.7]
ax = fig.add_axes([ax1_x, ax1_y, ax1_w, ax1_h], frame_on=True, facecolor = 'white',projection='3d')

ax.scatter(df['DC5'], df['DC2'], df['DC3'], color=df['Gcol'], s=1, marker="o")


#ax.view_init(5, 160)
ax.view_init(10, 140)

ax.set_xlabel('DC5')
ax.set_ylabel('DC2')
ax.set_zlabel('DC3')

ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])

for i,v in enumerate(class_1):
    ax.scatter([], [], [], color=cnames[i], s=20, label=v, marker="o")


legend = ax.legend(loc=2,bbox_to_anchor=(1.1, 0.8), frameon=False, fontsize=9,ncol=1)
legend.get_frame().set_facecolor('white')

ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))

plt.show()

