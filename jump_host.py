

import sys
import paramiko
import socket
import time 


def exe_cmd(jump_host_ip='c0008', cmd2exe):
    mid_cli = paramiko.SSHClient()  
    mid_cli.set_missing_host_key_policy(paramiko.AutoAddPolicy())  
    mid_cli.connect(hostname=jump_host_ip, timeout=30)
    print ('Connect to %s successfully !' % jump_host_ip)

    stdin, stdout, stderr = mid_cli.exec_command(cmd2exe)  
    print('stdout %s' % (''.join(stdout.readlines())))  
    error_msg = ''.join(stderr.readlines())
        
    #if error_msg != '':  
        #raise paramiko.ssh_exception(error_msg)
    #    pass  
    #else:  
    #    print ('run cmd %s successfully!' % (cmd2exe))

    stderr.close()  
    stdout.close()  


def main():
    exe_cmd('c0008',"python3 ~/webhook.py")

if __name__ == '__main__':
    main()

