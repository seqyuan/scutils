# scutils

single cell utils

```R
StackedVlnPlot(sdata, c('Retnlg', 'Pygl', 'Anxa1', 'Igf1r'), group.by = "seurat_clusters", pt.size=0)
```

![Rplot01](https://gitlab.com/seqyuan/scutils/-/raw/master/Rplot01.png)
